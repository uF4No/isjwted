## isJWTed ?
This module exports a middleware that can be used in Express applications to validate the presence of a valid JWT in the request header.

It requires the JWT key used to encode the tokens to be stored in the environment variable JWT_KEY (pending to make it parameterizable).
If token is successfully decoded, isjwted will add the payload as an object named jwtData in the request (eg. request.jwtData: {...}).
If there is any error decoding/verifying the token, isjwted will send the response back with code 401 (default). This can be changed to return an Error object to the next function via the env variable ISJWTED_SEND_RESPONSE

You can attach this middlewre to all the routes you need to protect from non logged users.

## Requirements
Node 8+
NPM
Env variable JWT_KEY.
(Optional) Env variable ISJWTED_SEND_RESPONSE (true/false)


# Dependencies
jsonwebtoken node module. 

Dev-dependecies:
mocha
chai
chai-http
