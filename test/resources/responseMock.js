
// Mock Reponse object from Express
const response = {
    status: (code) => {
      response.statusCode = code;
    return response;
  },
  send: (data) => {
    response.body = data
    return response;
  },
  
}

module.exports = response
