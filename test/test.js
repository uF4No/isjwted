/**
 * test/test.js
 * All test scenarios for isjwted.js
 */


const chai = require('chai');
const expect = chai.expect;

const jwt = require('jsonwebtoken')

process.env.JWT_KEY = 'MySuperSecretKeyToEncryptTokens'

const isjwted = require('../isjwted');

describe('Happy path', () => {

  it('should export a function with 3 params', () => {
    expect(isjwted).to.be.a('function');
    expect(isjwted.length).to.be.eq(3)
  });

  it('should add jwtData to request if valid token found', (done) => {

    //Mock request and response objects
    let requestMock = require('./resources/requestMock')
    let responseMock = require('./resources/responseMock')


    //add valid Token and data to request
    const validToken = jwt.sign({payload: 'This is the payload'}, process.env.JWT_KEY,{})
    requestMock.headers = {Authorization: `Bearer ${validToken}`}
    
    requestMock.body = {
      message: 'Hello!'
    }

    // call middleware
    isjwted(requestMock, responseMock);

    //assert it returns jwtData  
    expect(requestMock.jwtData).to.exist
    expect(requestMock.jwtData.payload).to.be.equal('This is the payload')
    //assert body is not modified
    expect(requestMock.body.message).to.be.eql('Hello!')

    done()

  })

})

describe('Token empty or no Authrization header', () => {

  it('should return response 401 if token empty and config ISJWTED_SEND_RESPONSE: Yes (default)', (done) => {
    //Mock request and response objects
    let requestMock = require('./resources/requestMock')
    let responseMock = require('./resources/responseMock')

    // add empty token
    requestMock.headers = {Authorization: `Bearer `}

    // change config
    process.env.ISJWTED_SEND_RESPONSE = 'Yes';
    
    // call middleware
    isjwted(requestMock, responseMock);

    // assertions
    expect(responseMock.statusCode).to.be.equal(401)
    expect(responseMock.body.error).to.exist
    expect(responseMock.body.error.name).to.be.eql('JsonWebTokenError')
    expect(responseMock.body.error.message).to.be.eql('jwt must be provided')

    done()

  })

  it('should return error object if token empty and config ISJWTED_SEND_RESPONSE: No', (done) => {
    //Mock request and response objects
    let requestMock = require('./resources/requestMock')
    let responseMock = require('./resources/responseMock')

    // add empty token
    requestMock.headers = {Authorization: `Bearer `}

    // change config
    process.env.ISJWTED_SEND_RESPONSE = 'No';
    
    // call middleware
    const resp = isjwted(requestMock, responseMock);

    // assertions
    expect(resp).to.exist
    expect(resp.error.name).to.be.eql('JsonWebTokenError')
    expect(resp.error.message).to.be.eql('jwt must be provided')

    done()

  })


  it('should return response 401 if no Auth header in request and config ISJWTED_SEND_RESPONSE: Yes (default)', (done) => {
    //Mock request and response objects
    let requestMock = require('./resources/requestMock')
    let responseMock = require('./resources/responseMock')
    
    // add empty token
    requestMock.headers = ''

    // change config
    process.env.ISJWTED_SEND_RESPONSE = 'Yes';

    // call middleware
    isjwted(requestMock, responseMock);

    expect(responseMock.body.error).to.exist
    expect(responseMock.statusCode).to.be.equal(401)
    expect(responseMock.body.error.name).to.be.eql('NoAuthorizationHeader')
    expect(responseMock.body.error.message).to.be.eql('No Authorization Header found in request')
    done()
  })


  it('should return error object if no Auth header in request and config ISJWTED_SEND_RESPONSE: No', (done) => {
    //Mock request and response objects
    let requestMock = require('./resources/requestMock')
    let responseMock = require('./resources/responseMock')
    
    // add empty token
    requestMock.headers = ''

    // change config
    process.env.ISJWTED_SEND_RESPONSE = 'No';

    // call middleware
    const resp = isjwted(requestMock, responseMock);

    // assertions
    expect(resp).to.exist
    expect(resp.error.name).to.be.eql('NoAuthorizationHeader')
    expect(resp.error.message).to.be.eql('No Authorization Header found in request')

    done()
  })
})



describe('Invalid token', () => {

  it('should return response 401 if invalid token and config ISJWTED_SEND_RESPONSE: Yes (default)', (done) => {

    //Mock request and response objects
    let requestMock = require('./resources/requestMock')
    let responseMock = require('./resources/responseMock')

    //add invalid token to request
    requestMock.headers = {Authorization: 'Bearer awd8208n02n8230d320n8dn0328nwadawdawdawdawdd023npd9212384029d23ob923b9b293ofob9f2b89f2b389fbl89lbf83bl8f3lb82lb89f32b2b8f2blf829'}    

    // change config
    process.env.ISJWTED_SEND_RESPONSE = 'Yes';

    // call middleware
    isjwted(requestMock, responseMock);

    // assertions
    expect(responseMock.statusCode).to.be.equal(401)
    expect(responseMock.body.error).to.exist
    expect(responseMock.body.error.name).to.be.eql('JsonWebTokenError')
    expect(responseMock.body.error.message).to.be.eql('jwt malformed')

    done()

  })

  it('should return error object if invalid token and config ISJWTED_SEND_RESPONSE: No', (done) => {

    //Mock request and response objects
    let requestMock = require('./resources/requestMock')
    let responseMock = require('./resources/responseMock')

    // change config
    process.env.ISJWTED_SEND_RESPONSE = 'No';


    //add invalid token to request
    requestMock.headers = {Authorization: 'Bearer awd8208n02n8230d320n8dn0328nwadawdawdawdawdd023npd9212384029d23ob923b9b293ofob9f2b89f2b389fbl89lbf83bl8f3lb82lb89f32b2b8f2blf829'}    

    // call middleware
    const resp = isjwted(requestMock, responseMock);

    // assertions
    expect(resp.error).to.exist
    expect(resp.error.name).to.be.eql('JsonWebTokenError')
    expect(resp.error.message).to.be.eql('jwt malformed')

    done()

  })

})
