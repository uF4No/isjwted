/**
 * isjwted.js
 * Exports an arrow funtion (synchronous) used as middleware by and express app. Requires process.env.JWT_KEY to validate the token.
 * Validates presence of a valid auth token in request header. 
 * Adds token payload to req.tokenData{}
 * Returns an error if token not provided or invalid
 */
const jwt = require('jsonwebtoken');


module.exports = (req, res, next) => {
  try{
    // validate presence of authorization header
    if(req.headers !== undefined && req.headers.Authorization !== undefined){
      //get the token from header. Remove 'Bearer ' with split()[].
      const token = req.headers.Authorization.split(" ")[1]
      //verify method verifies and decodes the token
      const decoded = jwt.verify(token, process.env.JWT_KEY)
      //add userData from the JWT to the request
      req.jwtData = decoded;
      res.status(200)
    }else{
      // if no authrization header, throw error
      const error = {
        name: "NoAuthorizationHeader",
        message: "No Authorization Header found in request"
      }
      throw error
    }
   
    
  }catch(err){
    const error = {
      error: {
        name: err.name,
        message: err.message
      }
    }
    let send_response = process.env.ISJWTED_SEND_RESPONSE || 'Yes' 
    if(send_response === 'Yes'){
      //send reponse with error details
      res.status(401).send(error)
    }else{
      //send error to next middleware/function
      return error;
    }

  }

}